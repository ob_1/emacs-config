;;;; my-dot-emacs --- This is my dot emacs file
;;;; Commentary:
;;;; Useful variables in Emacs
;;;; system-type
;;;; system-name
;;;; window-system
;;;; system-configuration
;;;; Code:

(add-to-list 'load-path "~/emacs")
(load-file "~/emacs/ob1.el")

;; load system-dependent stuff
(if window-system
    (load-file (concat "~/emacs/" (symbol-name window-system) ".el")))

;; My remappings
(defun ob-c-backspace (arg)
  (if (and
       (member (char-before (point)) '(?\( ?\[ ?\{ ?\" ?'))
       (member (char-before (1+ (point))) '(?\) ?\] ?\} ?\" ?')))
      (delete-char 1))
  (backward-delete-char-untabify (prefix-numeric-value arg)))

(defun ob-maybe-insert-key ()
  (interactive)
  (let* ((key (this-command-keys))
	 (match (member (char-before (1+ (point))) '(?\) ?\] ?\} ?\" ?'))))
       (if (and
	    match
	    (string-equal (char-to-string (car match)) key))
	   (forward-char)
	 (if (string-equal key "}")
	     (c-electric-brace nil)
	   (if (string-equal key ")")
	       (c-electric-paren nil)
	     ;; XXX: don't do this double insert in comments or strings
	     (if (or
		  (string-equal key "\"")
		  (string-equal key "'"))
		 (progn
		   (insert (this-command-keys))
		   (insert (this-command-keys))
		   (forward-char -1))
	       (insert key)))))))

(defun ob-context-line-break ()
  (interactive)
  (let ((prev-char (char-before (point))))
    (if (and
	 prev-char
	 (char-equal prev-char ?\{)
	 (char-equal (char-before (1+ (point))) ?\}))
	(progn
	  (insert "\n\n")
	  (beginning-of-line)
	  (c-indent-command)
	  (forward-line -1)
	  (c-indent-command)
	  (end-of-line))
      (if (and
	   prev-char
	   (char-equal prev-char ?\)))
	  (insert "\n")
	(c-context-line-break)))))

(defun parenthesize-region ()
  (interactive)
  (let* ((start (region-beginning))
	 (end (region-end)))
    (if start
	(progn
	  (goto-char start)
	  (insert "(")
	  (goto-char (1+ end))
	  (insert ")")))))

(require 'cc-mode)
(add-hook 'c-mode-common-hook
	  (lambda ()
	    (define-key c-mode-base-map "\C-m"
	      'c-context-line-break)
	    (define-key c-mode-map "\M-["
	      'c-beginning-of-defun)
	    (define-key c-mode-map "\M-]"
	      'c-end-of-defun)
	    (define-key c-mode-map "\C-cod"
	      'ob-strcpy-to-strdup)
	    (define-key c-mode-map "\C-coc"
	      'ob-strdup-to-strcpy)
	    (gtags-mode 1)
	    (setq c-hungry-delete-key nil)
	    ;; (setq skeleton-pair 't)
	    ;; (mapc (lambda (elt)
	    ;;	(add-to-list 'skeleton-pair-alist elt))
	    ;;       '((?\( _ ?\)) (?\[ _ ?\]) (?\{ _ ?\})
	    ;;	 (?\" _ ?\") (?' _ ?')))
	    ;; (mapc (lambda (key)
	    ;;	 (define-key c-mode-map key
	    ;;	   'ob-maybe-insert-key))
	    ;;       '(")" "]" "}" "\"" "'"))
	    ;; (mapc (lambda (key)
	    ;;	 (define-key c-mode-map key
	    ;;	   'skeleton-pair-insert-maybe))
	    ;;       '("(" "[" "{"))
	    (define-key c-mode-map "\C-m"
	      'ob-context-line-break)
	    (setq c-backspace-function 'ob-c-backspace)))

(add-hook 'text-mode-hook
	  (lambda ()
	    (auto-fill-mode 1)))

(add-hook 'sh-mode-hook
	  (lambda ()
	    (setq sh-basic-offset 8)
	    (local-set-key (kbd "C-.")
			   'bk-autofill-testcase-title)
	    (local-set-key "\C-cto" 'bk-testcase-or-die)
	    (local-set-key "\C-cta" 'bk-testcase-and-die)))

(defun rust-electric-key (key)
  (insert key)
  (company-indent-or-complete-common))

(add-hook 'rust-mode-hook
	  (lambda()
	    (racer-mode 1)
	    (electric-indent-mode -1)
	    (define-key rust-mode-map "}"
	      '(lambda () (interactive) (rust-electric-key "}")))
	    (define-key rust-mode-map ")"
	      '(lambda () (interactive) (rust-electric-key ")")))
	    (define-key rust-mode-map ";"
	      '(lambda () (interactive) (rust-electric-key ";")))
	    ))

(add-hook 'racer-mode-hook #'eldoc-mode)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(ansi-term-color-vector
   [unspecified "#151718" "#CE4045" "#9FCA56" "#DCCD69" "#55B5DB" "#A074C4" "#55B5DB" "#D4D7D6"])
 '(compilation-message-face (quote default))
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#839496")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(cursor-type (quote bar))
 '(custom-enabled-themes (quote (tao-yang)))
 '(custom-safe-themes
   (quote
    ("603a9c7f3ca3253cb68584cb26c408afcf4e674d7db86badcfe649dd3c538656" "40bc0ac47a9bd5b8db7304f8ef628d71e2798135935eb450483db0dbbfff8b11" "38ba6a938d67a452aeb1dada9d7cdeca4d9f18114e9fc8ed2b972573138d4664" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "d8f76414f8f2dcb045a37eb155bfaa2e1d17b6573ed43fb1d18b936febc7bbc2" "7b4d9b8a6ada8e24ac9eecd057093b0572d7008dbd912328231d0cada776065a" "5259c8b7e63f34e810b68385c940d69935536162e72f81ec80d7ff0a919dca6a" "b7b2cd8c45e18e28a14145573e84320795f5385895132a646ff779a141bbda7e" "3ac266781ee0ac3aa74a6913a1506924cad669f111564507249f0ffa7c5e4b53" "4f81886421185048bd186fbccc98d95fca9c8b6a401771b7457d81f749f5df75" "4d80487632a0a5a72737a7fc690f1f30266668211b17ba836602a8da890c2118" "28ec8ccf6190f6a73812df9bc91df54ce1d6132f18b4c8fcc85d45298569eb53" "0820d191ae80dcadc1802b3499f84c07a09803f2cb90b343678bdb03d225b26b" "94ba29363bfb7e06105f68d72b268f85981f7fba2ddef89331660033101eb5e5" "e726f73064b12668192c88c952293c37fda285fb2675ed2aa30c8c9b217a53ed" default)))
 '(ediff-split-window-function (quote split-window-horizontally))
 '(ediff-use-faces t)
 '(ediff-window-setup-function (quote ediff-setup-windows-plain))
 '(erc-email-userid "ob")
 '(fci-rule-color "#14151E")
 '(flycheck-clang-include-path
   (quote
    ("libc" "tommath" "tomcrypt/src/headers" "gui/tcltk/pcre/local/include")))
 '(flycheck-clang-warnings
   (quote
    ("all" "extra" "no-parentheses" "no-sign-compare" "no-pointer-sign" "no-missing-field-initializers")))
 '(highlight-changes-colors (quote ("#d33682" "#6c71c4")))
 '(highlight-symbol-colors
   (--map
    (solarized-color-blend it "#002b36" 0.25)
    (quote
     ("#b58900" "#2aa198" "#dc322f" "#6c71c4" "#859900" "#cb4b16" "#268bd2"))))
 '(highlight-symbol-foreground-color "#93a1a1")
 '(highlight-tail-colors
   (quote
    (("#073642" . 0)
     ("#546E00" . 20)
     ("#00736F" . 30)
     ("#00629D" . 50)
     ("#7B6000" . 60)
     ("#8B2C02" . 70)
     ("#93115C" . 85)
     ("#073642" . 100))))
 '(hl-bg-colors
   (quote
    ("#7B6000" "#8B2C02" "#990A1B" "#93115C" "#3F4D91" "#00629D" "#00736F" "#546E00")))
 '(hl-fg-colors
   (quote
    ("#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36")))
 '(inhibit-startup-echo-area-message "ob")
 '(magit-diff-use-overlays nil)
 '(nrepl-message-colors
   (quote
    ("#dc322f" "#cb4b16" "#b58900" "#546E00" "#B4C342" "#00629D" "#2aa198" "#d33682" "#6c71c4")))
 '(org-agenda-files (quote ("~/bk/TODO.org")))
 '(pos-tip-background-color "#073642")
 '(pos-tip-foreground-color "#93a1a1")
 '(show-paren-mode t)
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#073642" 0.2))
 '(term-default-bg-color "#002b36")
 '(term-default-fg-color "#839496")
 '(transient-mark-mode t)
 '(uniquify-buffer-name-style (quote forward) nil (uniquify))
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#d54e53")
     (40 . "goldenrod")
     (60 . "#e7c547")
     (80 . "DarkOliveGreen3")
     (100 . "#70c0b1")
     (120 . "DeepSkyBlue1")
     (140 . "#c397d8")
     (160 . "#d54e53")
     (180 . "goldenrod")
     (200 . "#e7c547")
     (220 . "DarkOliveGreen3")
     (240 . "#70c0b1")
     (260 . "DeepSkyBlue1")
     (280 . "#c397d8")
     (300 . "#d54e53")
     (320 . "goldenrod")
     (340 . "#e7c547")
     (360 . "DarkOliveGreen3"))))
 '(vc-annotate-very-old-color nil)
 '(weechat-color-list
   (quote
    (unspecified "#002b36" "#073642" "#990A1B" "#dc322f" "#546E00" "#859900" "#7B6000" "#b58900" "#00629D" "#268bd2" "#93115C" "#d33682" "#00736F" "#2aa198" "#839496" "#657b83")))
 '(xterm-color-names
   ["#073642" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#eee8d5"])
 '(xterm-color-names-bright
   ["#002b36" "#cb4b16" "#586e75" "#657b83" "#839496" "#6c71c4" "#93a1a1" "#fdf6e3"]))


;; XXX (load-file "~/emacs/sml-modeline.el")
(require 'sml-modeline)
(sml-modeline-mode 1)

;; from http://emacs-fu.blogspot.com/2009/12/scrolling.html
(setq
  scroll-margin 0
  scroll-conservatively 100000
  scroll-preserve-screen-position 1)

;;(menu-bar-right-scroll-bar)
; return always indents
(global-set-key "\C-m" (global-key-binding "\C-j"))

;; by Ellen Taylor, 2012-07-20
(defadvice shell (around always-new-shell)
  "Always start a new shell."
  (let ((buffer (generate-new-buffer-name "*shell*"))) ad-do-it))

(ad-activate 'shell)

(add-hook 'shell-mode-hook (lambda ()
			     (add-to-list
			      'shell-font-lock-keywords
			      '("^bk .*" . font-lock-builtin-face))
			     (add-to-list
			      'shell-font-lock-keywords
			      '("^bk .*LOCK.*" .
				font-lock-string-face))
			     (local-set-key
			      "\C-ccn"
			      '(lambda (n)
				 (interactive "p")
				 (insert "BK_CONFIG=checkout:none\! ")))))

;; some of my own stuff
(defun date ()
  "Insert the date in the current buffer"
  (interactive)
  (insert (chomp (shell-command-to-string "date +%Y-%m-%d"))))

(defun timestamp ()
  "Insert a timestamp in the current buffer"
  (interactive)
  (insert (chomp (shell-command-to-string "date +%Y-%m-%d\\ %H:%M"))))


(autoload 'markdown-mode "~/emacs/markdown-mode.el"
   "Major mode for editing Markdown files" t)

(add-to-list 'auto-mode-alist
	     '("\\.adoc\\'" . adoc-mode)
	     '("\\.md" . markdown-mode))

;; Use ibuffer to list buffers
(defalias 'list-buffers 'ibuffer)

;; make buffer switch command auto suggestions, also for find-file command
(ido-mode 1)

;; make ido display choices vertically
;;(setq ido-separator "\n")

;; display any item that contains the chars you typed
(setq ido-enable-flex-matching t)

;; Make emacs simple
;; Most of it from: http://bzg.fr/emacs-strip-tease.html
;; ---------- start ----------
;; Prevent the cursor from blinking
(blink-cursor-mode 0)
;; Don't use messages that you don't read
(setq initial-scratch-message "")
(setq inhibit-startup-message t)
;; Don't let Emacs hurt your ears
(setq visible-bell t)
(tool-bar-mode 0)
(menu-bar-mode 0)

;; Alternatively, you can paint your mode-line in White but then
;; you'll have to manually paint it in black again
;; (custom-set-faces
;;  '(mode-line-highlight ((t nil)))
;;  '(mode-line ((t (:foreground "white" :background "white"))))
;;  '(mode-line-inactive ((t (:background "white" :foreground "white")))))

;; You need to set `inhibit-startup-echo-area-message' from the
;; customization interface:
;; M-x customize-variable RET inhibit-startup-echo-area-message RET
;; then enter your username
(setq inhibit-startup-echo-area-message "ob")

;;(toggle-fullscreen)
;; Who uses the bar to scroll?
(scroll-bar-mode 0)

;;; Remember how we were set up
(desktop-save-mode 1)

;;;;;;;;;; ---- end ----

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ediff-current-diff-A ((t (:background "misty rose" :foreground "#080808"))))
 '(ediff-current-diff-B ((t (:background "light green" :foreground "#080808"))))
 '(ediff-even-diff-A ((t (:background "light gray"))))
 '(ediff-even-diff-B ((t (:background "light gray"))))
 '(ediff-fine-diff-A ((t (:background "pink" :foreground "#080808" :weight bold))))
 '(ediff-fine-diff-B ((t (:background "lime green" :foreground "#080808" :weight bold))))
 '(ediff-odd-diff-A ((t (:background "light gray"))))
 '(ediff-odd-diff-B ((t (:background "light gray")))))

(set-default 'cursor-type 'box)
