;; META TODO: I need to split this file in bk.el and bitmover.el such
;; that the bk crud is not mixed with bitmover stuff (like testcases)

(setq bk-compilation-command "")

(defconst bk-version "0.1"
  "Version number for this version of bk mode.")

(require 'cl)
(require 'ediff)
(load-file "~/emacs/bk-changes-mode.el")

;;(append 'compilation-finish-functions (lambda (buf result)
;;  (shell-command
;;   (format
;;    "growlnotify -s -m'Compilation of %s finished with result .'" buf))))

(defun gfile-to-xfile (gfile x)
  (concat (file-name-directory gfile)
	  (file-name-as-directory "SCCS")
	  (concat (format "%s." x) (file-name-nondirectory gfile))))

(defun bk-compile ()
  (interactive)
  (cd (format "%s/src" (bk-root default-directory)))
  (if (string-equal window-system "w32")
      (progn
	(compile "cmd.exe /C \"sh.exe --login pwd && make build && sh build p image\""))
    (compile "cd `bk -P root`/src && make -j12 image")))

;;(global-set-key "\C-z" 'bk-compile)

(defun chomp (str)
  (replace-regexp-in-string "[\n\r]+$" "" str nil))

(defun bk-src-tree-p (path)
  (or
   (string=
    "lm@lm.bitmover.com|ChangeSet|19990319224848|02682|B:1000:6a5ce40345b2dee1"
    (chomp (shell-command-to-string (format "bk sh -c \"cd \
	   %s && bk -P id\"" path))))
   (string= "lm@lm.bitmover.com|ChangeSet|19990319224848|02682"
	   (chomp (shell-command-to-string (format "bk sh -c \"cd \
	   %s && bk id\"" path))))))

(defun tcl-src-tree-p (path)
  (let ((id (chomp (shell-command-to-string (format "cd %s && bk id" path)))))
    (or
     (string=
      "rjohnson@tcl.tk|ChangeSet|19980326144559|00000|ee88617e412f5f18" id)
     (string=
      "rjohnson@tcl.tk|ChangeSet|19980326144559|00000|42af23f84408573b" id)
     (string=
      "ob@dirac.bitmover.com|ChangeSet|20100807051800|12017|6035706611c1c234"
      id))))


(defun tk-src-tree-p (path)
  (string= "rjohnson@tcl.tk|ChangeSet|19980401093739|00000|c9750ce61f3e0879"
	   (chomp (shell-command-to-string (format "cd %s && bk id" path)))))

;; (defun bk-open (file rev)
;;   "Open a version of a file in a buffer"
;;   (interactive "fFile Name: \nMRev: ")
;;   (set-window-point
;;    (display-buffer
;;     (switch-to-buffer (bk-get-to-buffer file rev))) 1))

;; XXX: Temporary hack... just runs bk get
(defun dired-bk-get (&optional file-list)
  "Run bk get on the current directory."
  (shell-command "bk get"))

(defun bk-cscope ()
  "Make cscope.out for the BK tree"
  (interactive)
  (if (bk-src-tree-p default-directory)
      (progn
	(message "building cscope db...")
	(let ((srcdir (concat (bk-root default-directory) "/src")))
	  (cd srcdir)
	  (with-temp-file cscope-index-file
	    (insert (shell-command-to-string
		     (concat "find . -name \"*.[ch]\" | grep -v SCCS | "
			    "egrep -v '^./(gnu|gui|tomcrypt|tommath|win32)'")))
	    (goto-char (point-min))
	    (replace-regexp "^\./" ""))
	  (shell-command "cscope -b")
	  (message "done")))
    (message "Not in a BitKeeper tree")))

(defun bk-etags ()
  "Make tags for the BK tree"
  (interactive)
  (if (bk-src-tree-p default-directory)
      (progn
	(message "building tags...")
	(setenv "BK_USEMSYS" "1")
	(call-process-shell-command
	 (format
	  (concat "bk sh -c \"cd %s/src && bk sfiles -g | "
	   "egrep -v '(gnu/diffutils|gnu/patch|win32/pub)' | "
	   "egrep '.*\\.([ch]|tcl)' | xargs etags -a\"")
	  (bk-root default-directory)))
	(message "done")
	(tags-reset-tags-tables)
	(visit-tags-table-buffer
	 (format "%s/src/TAGS" (bk-root default-directory))))
    (message "Not in a BK tree")))

(defun bk-gtags ()
  "Make gtags fro BK tree"
  (interactive)
  (if (bk-src-tree-p default-directory)
      (progn
	(message "building gtags...")
	(call-process-shell-command
	 (format
	  (concat "cd %s/src && gtags")
	  (bk-root default-directory)))
	(message "done")
	(tags-reset-tags-tables)
	(gtags-mode))
    (message "Not in a BK tree")))

(defun tcl-etags ()
  "Make tags for the Tcl tree"
  (interactive)
  (if (tcl-src-tree-p default-directory)
      (progn
	(call-process-shell-command
	 (format
	  (concat "cd %s && bk sfiles -g | egrep '.*\\.([ch]|tcl)' | "
	   "grep -v tclDecls.h | xargs etags") (bk-root default-directory)))
	(tags-reset-tags-tables)
	(visit-tags-table-buffer
	 (format "%s/TAGS" (bk-root default-directory))))))

(defun tk-etags ()
  "Make tags for the Tk tree"
  (interactive)
  (if (tk-src-tree-p default-directory)
      (progn
	(call-process-shell-command
	 (format
	  (concat "cd %s && bk sfiles -g | "
	   "egrep '.*\\.([ch]|tcl)' | xargs etags")
	  (bk-root default-directory)))
	(tags-reset-tags-tables)
	(visit-tags-table-buffer
	 (format "%s/TAGS" (bk-root default-directory))))))

(defun make-etags ()
  "Make tags for a tree"
  (interactive)
  (progn
    (call-process-shell-command "find . -name \\*.[ch] | xargs etags")
    (tags-reset-tags-tables)
    (visit-tags-table-buffer "TAGS")))


(defun nrt ()
  "Run BK newrevtool on the current file"
  (interactive)
  (let ((filename (buffer-file-name (current-buffer))))
    (if filename
	(start-process
	 "newrevtool" "newrevtool"
	 "/Users/ob/bk/i/newrevtool/bk" "newrevtool"
	 (format "-@%s" (line-number-at-pos))
	 (expand-file-name filename))
      (start-process
       "newrevtool" "newrevtool" "bk" "-R" "newrevtool"))))


(defun opendiff ()
  "Run Apple's FileMerge on the current file"
  (interactive)
  (let ((filename (buffer-file-name (current-buffer))))
    (if filename
	(if (string-equal
	     (shell-command-to-string (format "bk diffs %s" filename)) "")
	    (message "Buffer not modified")
	  (progn
	    (shell-command (format "bk get -qr+ -p '%s' > /tmp/original"
				   filename))
	    (start-process
	     "opendiff" "opendiff"
	     "opendiff" "/tmp/original" filename ))))))

;;; Convenient shortcuts for writing BitKeeper testcases

;; TODO: handle shell quoting of the string automatically.
(defun bk-autofill-testcase-title ()
  (interactive)
  (let ((line (thing-at-point 'line)))
    (beginning-of-line)
    (insert "echo $N ")
    (end-of-line)
    (insert-char ?. (- 61 (length (chomp line))))
    (insert "$NL\n\necho OK\n")
    (forward-line -2)
    (beginning-of-line)))

(define-skeleton bk-testcase-or-die
  "Insert the end of a test line"
  "File: "
  ">" str " 2>&1 || {" ?\n
  > "echo failed" ?\n
  > "cat " str ?\n
  > "exit 1" ?\n
  "}"
  ?\n )

(define-skeleton bk-testcase-and-die
  "Insert the end of a test line"
  "File: "
  ">" str " 2>&1 && {" ?\n
  > "echo failed" ?\n
  > "cat " str ?\n
  > "exit 1" ?\n
  "}"
  ?\n )

(define-skeleton bk-getopt
  "Insert a standard getopt line."
  >"int	c;" ?\n
  ?\n
  > "while ((c = getopt(ac, av, \"\")) != -1) {" ?\n
  > "switch (c) {" ?\n
  > "default:" ?\n
  > "system(\"bk help -s\");" ?\n
  > "return (1);" ?\n
  > "}" ?\n
  > "}" ?\n
  )

(defun pastie ()
  (interactive)
  (let ((format (case major-mode
		  ('emacs-lisp-mode "lisp")
		  ('shell-mode "bash")
		  ('sh-mode "bash")
		  (otherwise
		   (car (split-string (format "%s" major-mode) "-"))))))
    (shell-command-on-region (region-beginning) (region-end)
			     (format "pastie -f %s -t '%s'"
				     format (buffer-name)))
    (with-current-buffer (get-buffer "*Shell Command Output*")
      (kill-new (chomp (buffer-string))))))

(defun bk-check-in (dir)
  (interactive (list (read-file-name "Directory: " nil "" t)))
  (let ((dired-buffers dired-buffers))
    (setq dir (file-name-as-directory (expand-file-name dir)))
    (or (file-directory-p dir)
	(error "not a directory: %s" dir))
    (switch-to-buffer (get-buffer-create "*BK Check In*"))
    ;; TODO: see if there is a running sfiles and kill it
    (widen)
    (kill-all-local-variables)
    (setq buffer-read-only nil)
    (erase-buffer)
    (setq default-directory dir)
    (shell-command "bk sfiles -cgx | xargs ls -l" (current-buffer))
    (dired-virtual-mode)
    ;; see diffs
    (local-set-key "\C-ccd" '(lambda ()
			       (interactive)
			       (bk-diffs (thing-at-point 'filename))))))

