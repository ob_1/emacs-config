(eval-when-compile (require 'cl))

(defgroup bk-changes-mode ()
  "Major mode for viewing the output of bk changes."
  :version "1.0"
  :group 'tools
  :group 'bk-changes)

(easy-mmode-defmap bk-changes-mode-map
  '(("n" . bk-changes-cset-next)
    ("p" . bk-changes-cset-prev)
    ("\C-m" . bk-changes-review-changeset)
    ("q" . quit-window))
  "Basic keymap for `bk-changes-mode', bound to various prefix keys.")

(defun bk-changes-review-changeset ()
  (interactive)
  (save-excursion
    (progn
      (search-backward-regexp "^ChangeSet@\\([0-9\.]+\\)" (end-of-line) t)
      (bk-review-internal default-directory (match-string 1)))))

(defun bk-changes-cset-next ()
  (interactive)
  (search-forward-regexp "^ChangeSet" (end-of-line) t)
  (move-beginning-of-line nil))

(defun bk-changes-cset-prev ()
  (interactive)
  (search-backward-regexp "^ChangeSet" (beginning-of-line) t)
  (move-beginning-of-line nil))

(defface bk-changes-header
  '((((class color) (min-colors 88) (background light))
     :background "grey85")
    (((class color) (min-colors 88) (background dark))
     :background "grey45")
    (((class color) (background light))
     :foreground "blue1" :weight bold)
    (((class color) (background dark))
     :foreground "green" :weight bold)
    (t :weight bold))
  "`bk-changes-header' face inherited by hunk and index header faces."
  :group 'bk-changes-mode)

(defface bk-changes-selected-comment
 '((((class color) (min-colors 88) (background light))
    :background "grey85")
   (((class color) (min-colors 88) (background dark))
    :background "MediumPurple")
   (((class color) (background light))
    :foreground "blue1" :weight bold)
   (((class color) (background dark))
    :foreground "orange" :weight bold)
   (t :weight bold))
 "`bk-changes-selected-comment' face used for selected comment."
 :group 'bk-changes-mode)

(defvar bk-changes-header-face 'bk-changes-header)

(defvar bk-changes-font-lock-keywords
  `(("^ChangeSet.*" . bk-changes-header-face)))

(defconst bk-changes-font-lock-defaults
  '(bk-changes-font-lock-keywords t nil nil nil (font-lock-multiline . nil)))

;; (defvar bk-changes-outline-regexp
;;   "\\([*+][*+][*+] [^0-9]\\|@@ ...\\|\\*\\*\\* [0-9].\\|--- [0-9]..\\)")

(define-derived-mode bk-changes-mode fundamental-mode "Bk-Changes"
  ""
  (set (make-local-variable 'font-lock-defaults) bk-changes-font-lock-defaults)
;;   (set (make-local-variable 'outline-regexp) bk-changes-outline-regexp)
;;   (set (make-local-variable 'imenu-generic-expression)
;;        bk-changes-imenu-generic-expression)
  (setq buffer-read-only t))

(provide 'bk-changes-mode)
