;;; ob1 --- ob1's customizations
;;; Commentary:
;;; Customizations so that I can use the damned thing
;; These should all be portable, system-dependent stuff
;; goes in (eval window-system).el

;; (global-linum-mode)

;;; Code:

;; Package initialization
(require 'package)

(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives
    '("marmalade" .
      "http://marmalade-repo.org/packages/"))
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))

(package-initialize)
;; Package Done

(global-set-key (kbd "S-C-<down>") 'shrink-window)
(global-set-key (kbd "S-C-<up>") 'enlarge-window)
(global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)

;; it seems my theme colors don't work on diffs if diff-mode
;; has not been loaded
(require 'diff-mode)
(require 'uniquify)

;; (load-file "~/emacs/theme.el")

;; Typography section
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

;; enable typopunct in adoc mode
(require 'typopunct)
(add-hook 'adoc-mode-hook 'my-adoc-init)
(defun my-adoc-init ()
  (require 'typopunct)
  (typopunct-change-language 'english)
  (typopunct-mode 1))

;; handle dash, en-dash, em-dash, and minus signs
(defconst typopunct-minus (decode-char 'ucs #x2212))
(defconst typopunct-pm    (decode-char 'ucs #xB1))
(defconst typopunct-mp    (decode-char 'ucs #x2213))
(defconst typopunct-hard-hyphen (decode-char 'ucs #x2011))
(defadvice typopunct-insert-typographical-dashes
  (around minus-or-pm activate)
  (cond
   ((or (eq (char-before) typopunct-em-dash)
	(looking-back "\\([[:blank:]]\\|^\\)\\^"))
    (delete-char -1)
    (insert typopunct-minus))
   ((looking-back "[^[:blank:]]\\^")
    (insert typopunct-minus))
   ((or (eq (char-before) typopunct-em-dash)
	(looking-back "\\\\"))
    (delete-char -1)
    (insert typopunct-hard-hyphen))
   ((looking-back "+/")
    (progn (replace-match "")
	   (insert typopunct-pm)))
   (t ad-do-it)))

(defun typopunct-insert-mp (arg)
  (interactive "p")
  (if (and (= 1 arg) (looking-back "-/"))
      (progn (replace-match "")
	     (insert typopunct-mp))
    (self-insert-command arg)))
(define-key typopunct-map "+" 'typopunct-insert-mp)

;; handle ellipsis
(defconst typopunct-ellipsis (decode-char 'ucs #x2026))
(defconst typopunct-middot   (decode-char 'ucs #xB7)) ; or 2219
(defun typopunct-insert-ellipsis-or-middot (arg)
  "Change three consecutive dots to a typographical ellipsis mark."
  (interactive "p")
  (cond
   ((and (= 1 arg)
	 (eq (char-before) ?^))
    (delete-char -1)
    (insert typopunct-middot))
   ((and (= 1 arg)
	 (eq this-command last-command)
	 (looking-back "\\.\\."))
    (replace-match "")
    (insert typopunct-ellipsis))
   (t
    (self-insert-command arg))))
(define-key typopunct-map "." 'typopunct-insert-ellipsis-or-middot)

;; multiplication sign
(defconst typopunct-times (decode-char 'ucs #xD7))
(defun typopunct-insert-times (arg)
  (interactive "p")
  (if (and (= 1 arg) (looking-back "\\([[:blank:]]\\|^\\)\\^"))
      (progn (delete-char -1)
	     (insert typopunct-times))
    (self-insert-command arg)))
(define-key typopunct-map "x" 'typopunct-insert-times)

;; tell ispell about typographic quotes
(quote (("english" "[[:alpha:]]" "[^[:alpha:]]" "['’]" t ("-d" "en_US") nil utf-8)))


;; (load-file "~/emacs/gnuplot.el")
;; (load-file "~/emacs/make-regexp.el")
;; (load-file "~/emacs/flex-mode.el")
;; (load-file "~/emacs/bison-mode.el")

;; Make my life easier for moving between windows
(defun other-window-previous ()
  "Move to the previous window."
  (interactive)
  (other-window (- 1)))

(global-set-key "\C-x\C-n" 'other-window)
(global-set-key "\C-xn" 'other-window)
(global-set-key "\C-x\C-p" 'other-window-previous)
(global-set-key "\C-xp" 'other-window-previous)
(global-set-key "\C-x\C-o" 'other-window)

;; Some time savers
;;
;; M-x bound to C-x C-m
(global-set-key "\C-x\C-m" 'execute-extended-command)
;; I don't send mail with emacs, and I keep hitting stuff by
;; mistake.
(global-set-key "\C-xm" 'execute-extended-command)
(global-set-key "\C-c\C-m" 'execute-extended-command)

;; scrolling in place is really useful for reading code
(global-set-key
 [S-up] '(lambda (n) "Scroll up in place."
	   (interactive "p")
	   (let* ((p (point)))
	     (progn
	       (scroll-down n)
	       (goto-char p)))))

(global-set-key
 [S-down] '(lambda (n) "Scroll down in place."
	     (interactive "p")
	     (let* ((p (point)))
	       (progn
		 (scroll-up n)
		 (goto-char p)))))
(global-set-key
 [M-up] '(lambda (n) "Scroll down."
	   (interactive "p")
	   (scroll-down n)))
(global-set-key
 [M-down] '(lambda (n) "Scroll up."
	   (interactive "p")
	   (scroll-up n)))

;; make f2 a toggle for selective-display (C-x $)
(defun toggle-selective-display ()
  "Toggle selective display."
  (interactive)
  (set-selective-display (if selective-display nil 1)))
(global-set-key [f2] 'toggle-selective-display)

;; customize commint
(defun ob-comint-handler (proc string)
  "PROC: the proc.  STRING: The string."
  (if (string-match "^[ 	]*man \\(.*\\)" string)
      (progn
	(man (match-string 1 string))
	(comint-simple-send proc ""))
    (if (string-match "^[ 	]*bk[ 	]+help[ 	]+\\(.*\\)" string)
	(progn
	  (bk-help (match-string 1 string))
	  (comint-simple-send proc ""))
      (comint-simple-send proc string))))

;; trap "man" and "bk help" commands in shell mode and run emacs
;; functions instead.
(setq comint-input-sender 'ob-comint-handler)

(add-hook 'comint-mode-hook (lambda ()
			       (local-set-key
				"\M-p"
				'comint-previous-matching-input-from-input)
			       (local-set-key
				"\M-n"
				'comint-next-matching-input-from-input)))

(load-file "~/emacs/develock.el")
(require 'develock)

(if (featurep 'develock)
    ;; make it easy to turn off develock ("\C-x\C-q" conflicts with gdb)
    (global-set-key "\C-x\C-g" 'develock-mode))

;; I like fontlock on
(if (featurep 'font-lock)
    (global-font-lock-mode 1))

;; highlight matching parenthesis
(show-paren-mode t)
(setq show-paren-style 'parenthesis)

(defun R ()
  "Run R."
  (interactive)
  (require 'ess-site)
  (R))

;; make the default size a bit larger
;;(add-to-list 'default-frame-alist '(width . 85))
;;(add-to-list 'default-frame-alist '(height . 50))


;; Org-mode stuff
;; Org-mode
;; The following lines are always needed.  Choose your own keys.
(require 'org-install)
(require 'ob-python)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-hide-leading-stars t)
;; (setq org-log-done t)
;; ;; handy shortcut

;; (add-hook 'c++-mode-hook 'irony-mode)
;; (add-hook 'c-mode-hook 'irony-mode)
;; (add-hook 'objc-mode-hook 'irony-mode)

;; ;; replace the `completion-at-point' and `complete-symbol' bindings in
;; ;; irony-mode's buffers by irony-mode's function
;; (defun my-irony-mode-hook ()
;;   (define-key irony-mode-map [remap completion-at-point]
;;     'irony-completion-at-point-async)
;;   (define-key irony-mode-map [remap complete-symbol]
;;     'irony-completion-at-point-async))
;; (add-hook 'irony-mode-hook 'my-irony-mode-hook)
;; (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)


(add-to-list 'load-path "~/emacs/session/lisp")
(require 'session)
(add-hook 'after-init-hook 'session-initialize)

(defun chomp (str)
  (replace-regexp-in-string "[\n\r]+$" "" str nil))

;; experimental BitKeeper backend
;; (let* ((bin (chomp (shell-command-to-string "bk bin")))
;;       (vc-bk (format "%s/contrib/vc-bk.el" bin)))
;;   (if (file-exists-p vc-bk)
;;       (load-file vc-bk)))


(load-file "~/bk/dev-emacs/src/gui/ide/emacs/vc-bk.el")
(add-to-list 'vc-handled-backends 'Bk)

(load-file "~/emacs/bk.el")
;; (load-file "~/emacs/xcscope.el")
(require 'xcscope)

(add-hook 'tcl-mode-hook 'turn-on-font-lock)

;; cc-mode customization
(require 'cc-mode)
(require 'imenu)
;; enable function names in the modline
(setq cc-imenu-c-generic-expression
  `(
    ;; Special case to match a line like `main() {}'
    ;; e.g. no return type, not even on the previous line.
    (nil
     ,(concat
       "^"
       "\\([" c-alpha "_][" c-alnum "_]*\\)" ; match function name
       "[ \t]*("			      ; see above, BUT
       "[ \t]*\\([^ \t(*][^)]*\\)?)"	      ; the arg list must not start
       "[ \t]*[^ \t;(]"			      ; with an asterisk or parentheses
       ) 1)
    ;; General function name regexp
    (nil
     ,(concat
       "^\\<"; line MUST start with word char
       ;; \n added to prevent overflow in regexp matcher.
       ;; http://lists.gnu.org/archive/html/emacs-pretest-bug/2007-02/msg00021.html
       "[^()\n:]*"			      ; no parentheses before
       "[^" c-alnum "_]"		      ; match any non-identifier char
       "\\([" c-alpha "_][" c-alnum "_]*\\)" ; match function name
       "\\([ \t\n]\\|\\\\\n\\)*("	      ; see above, BUT the arg list
       "\\([ \t\n]\\|\\\\\n\\)*\\([^ \t\n(*][^)]*\\)?)" ; must not start
       "\\([ \t\n]\\|\\\\\n\\)*[^ \t\n;(]"    ; with an asterisk or parentheses
       ) 1)))

(which-func-mode 1)

(add-hook 'c-mode-common-hook
	  (lambda ()
	    (font-lock-add-keywords nil
				    '(("\\<\\(FIXME\\|TODO\\|BUG\\):" 1 font-lock-warning-face t)))))

(add-hook 'c-mode-hook
	  (lambda ()
	    (font-lock-add-keywords nil
				    '(("\\<\\(REVIEW\\):" 1
				       font-lock-warning-face t)))
	    (setq-local indent-tabs-mode t)))

(add-hook 'javascript-mode-hook
	  (lambda ()
	    (setq-local indent-tabs-mode t)))

(add-hook 'java-mode-hook (lambda ()
				   (setq-local indent-tabs-mode nil)))


;; shell mode for BitKeeper's testcases. I could come up with one
;; regexp that considers both "t.foo" and "~/foo/t.foo", but I'm too
;; lazy so I just push two regexes.
(push '("/t\\.[^/]+$" . sh-mode) auto-mode-alist)
(push '("/g\\.[^/]+$" . sh-mode) auto-mode-alist)
(push '("^t\\.[^/]+$" . sh-mode) auto-mode-alist)
(push '("^g\\.[^/]+$" . sh-mode) auto-mode-alist)
;; C mode for L code
(push '("\\.l$" . c-mode) auto-mode-alist)
;; ObjC mode for Cappuccino code
(push '("\\.j$" . objc-mode) auto-mode-alist)
;; fundamental-mode for SCCS sfiles (sigh, prefix? really?)
(push '("/s\\.[^/]+$" . fundamental-mode) auto-mode-alist)
(setq mac-option-modifier 'meta)
(transient-mark-mode t)
;; diff mode
;; turn off auto-refine mode, the colors are annoying.
(setq diff-mode-hook (lambda ()
			 (setq diff-auto-refine-mode nil)))
(setq ediff-split-window-function 'split-window-horizontally)
;; don't start a new frame
(setq ediff-window-setup-function 'ediff-setup-windows-plain)

;; make Larry happy about C style
(c-add-style "bitmover" '("bsd"
			  (c-basic-offset . 8)
			  (c-block-comment-prefix . "*")
			  (c-tab-always-indent . nil)
			  (c-offsets-alist
			   (comment-intro . 0)
			   (case-label . 4)
			   (statement-case-intro . 4)
			   (arglist-cont-nonempty . *))))

;; Python
(setq python-mode-hook (lambda ()
			 (setq show-trailing-whitespace t)))


(add-to-list 'load-path "~/bk/dev-emacs/src/gui/ide/emacs")
(require 'bk-emacs)

(require 'iedit)
(define-key global-map (kbd "C-;") 'iedit-mode)

;; (load-file "~/emacs/diary.el")
(unless (require 'moinmoin-mode nil 'noerror)
  (package-install 'moinmoin-mode))
(require 'moinmoin-mode)

(setq tab-always-indent 'complete)

;; (require 'autopair)
;; (autopair-global-mode 1)
;; (setq autopair-autowrap t)
;; (setq autopair-blink nil)

;; See http://bc.tech.coop/blog/071001.html
(server-start)
(add-hook 'after-init-hook 'server-start)
;; (add-hook 'server-done-hook
;; 	  (lambda ()
;; 	    (shell-command
;; 	     "screen -r -X select `cat ~/.emacsclient-caller`")))

;; Enable SRecode (Template management) minor-mode.
;; (global-srecode-minor-mode 1)

;; (load-file "~/emacs/myslime.elc")


;;(add-to-list 'load-path "~/.emacs.d/")
;;(require 'auto-complete-config)
;; (add-to-list 'ac-dictionary-directories "~/.emacs.d//ac-dict")
;; (ac-config-default)

;;(load-file "~/emacs/emacs-clang-complete-async/auto-complete-clang-async.el")
;;(require 'auto-complete-clang-async)

;;(defun ac-cc-mode-setup ()
;;  (setq ac-clang-complete-executable "~/.emacs.d/clang-complete")
;;  (setq ac-sources '(ac-source-clang-async))
;;  (ac-clang-launch-completion-process))

;;(defun my-ac-config ()
;;  (add-hook 'c-mode-common-hook 'ac-cc-mode-setup)
;;  (add-hook 'auto-complete-mode-hook 'ac-common-setup)
;;  (global-auto-complete-mode t))

;;(my-ac-config)

(defmacro defconstant (symbol initvalue &optional docstring)
  `(defconst ,symbol ,initvalue ,docstring))

(defmacro defparameter (symbol &optional initvalue docstring)
  `(progn
     (defvar ,symbol nil ,docstring)
     (setq   ,symbol ,initvalue)))

(defparameter th-frame-config-register ?°
  "The register which is used for storing and restoring frame
configurations by `th-save-frame-configuration' and
`th-jump-to-register'.")

(defun th-save-frame-configuration (arg)
  "Stores the current frame configuration in register
`th-frame-config-register'. If a prefix argument is given, you
can choose which register to use."
  (interactive "P")
  (let ((register (if arg
		      (read-char "Which register? ")
		    th-frame-config-register)))
    (frame-configuration-to-register register)
    (message "Frame configuration saved in register '%c'."
	     register)))

(defun th-jump-to-register (arg)
  "Jumps to register `th-frame-config-register'. If a prefix
argument is given, you can choose which register to jump to."
  (interactive "P")
  (let ((register (if arg
		      (read-char "Which register? ")
		    th-frame-config-register)))
    (jump-to-register register)
    (message "Jumped to register '%c'."
	     register)))

(global-set-key (kbd "<f5>")
		'th-save-frame-configuration)

(global-set-key (kbd "<f6>")
		'th-jump-to-register)

(add-hook 'after-init-hook #'global-flycheck-mode)
(add-hook 'after-init-hook 'global-company-mode)

(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony))

;; (optional) adds CC special commands to `company-begin-commands' in order to
;; trigger completion at interesting places, such as after scope operator
;;     std::|
(add-hook 'irony-mode-hook 'company-irony-setup-begin-commands)

(global-set-key (kbd "TAB") #'company-indent-or-complete-common) ;
(setq company-tooltip-align-annotations t)

;; web-mode

(defun my-web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-code-indent-offset 2))
(add-hook 'web-mode-hook  'my-web-mode-hook)
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))

;; go-mode

(require 'go-mode)
(add-hook 'before-save-hook 'gofmt-before-save)
(add-hook 'go-mode-hook '(lambda()
                           (local-set-key (kbd "C-c C-r")
                                          'go-remove-unused-imports)))
(add-hook 'go-mode-hook '(lambda()
                           (local-set-key (kbd "C-c C-g") 'go-goto-imports)))

(add-hook 'go-mode-hook '(lambda()
                           (local-set-key (kbd "C-c C-f") 'gofmt)))
(add-hook 'go-mode-hook '(lambda()
                           (local-set-key (kbd "C-c C-k") 'godoc)))
(add-hook 'go-mode-hook '(lambda()
                           (local-set-key (kbd "M-.") 'godef-jump)))
(load "$GOPATH/src/golang.org/x/tools/cmd/oracle/oracle.el")
(add-hook 'go-mode-hook 'go-oracle-mode)
(add-to-list 'load-path "~/gocode/src/github.com/dougm/goflymake")
(require 'go-flymake)
(require 'go-flycheck)
(add-hook 'go-mode-hook 'company-mode)
(add-hook 'go-mode-hook (lambda ()
  (set (make-local-variable 'company-backends) '(company-go))
  (company-mode)))



;; elm-mode
(require 'elm-mode)
(with-eval-after-load 'company
  (add-to-list 'company-backends 'company-elm))
(add-hook 'elm-mode-hook #'elm-oracle-setup-completion)


(global-set-key (kbd "C-x g") 'magit-status)

;; Disable tramp (We hates it my preciousss, yes we do)
(setq tramp-mode nil)

(provide 'ob1)
;;; ob1.el ends here
