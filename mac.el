;;; mac --- Summary
;;; Commentary:
;;;   Set a good programming font
;;; Code:

;; Inconsolata, I choose you!
;;

(set-frame-font
  "-apple-Inconsolata-medium-normal-normal-*-12-*-*-*-m-0-iso10646-1" nil t)

;; (set-frame-font
;;  "-*-Hack-normal-normal-normal-*-10-*-*-*-m-0-iso10646-1" nil t)

;; (set-frame-font
;;  "-*-Consolas-normal-normal-normal-*-11-*-*-*-m-0-iso10646-1" nil t)

(setq gtags-suggested-key-mapping t)
(load-file "~/emacs/gtags.el")
(autoload 'gtags-mode "gtags" "" t)

(global-set-key (kbd "A-`") 'other-frame)

(defun toggle-fullscreen ()
  "Toggle full screen."
  (interactive)
  (set-frame-parameter
     nil 'fullscreen
     (when (not (frame-parameter nil 'fullscreen)) 'fullboth)))

(provide 'mac)
;;; mac.el ends here
