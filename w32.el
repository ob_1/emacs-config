;; setuff for windows

(set-default-font
 "-outline-Consolas-normal-r-normal-normal-14-97-96-96-c-*-iso8859-1")
(add-to-list 'load-path "~/emacs/color-theme")
;; For msys-shell
(setq w32shell-msys-bin "c:/build/buildenv/bin")
(setq w32shell-cygwin-bin "c:/cygwin/bin")
