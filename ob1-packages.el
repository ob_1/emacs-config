;;; ob1-packages --- Packages needed for my emacs config
;;; Commentary:
;;; Code:

(package-install 'sml-modeline)
(package-install 'gnuplot)
(package-install 'screen-lines)
(package-install 'moinmoin-mode)
(package-install 'graphviz-dot-mode)

(provide 'ob1-packages)
;;; ob1-packages.el ends here
